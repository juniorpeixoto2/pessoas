<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Request;
use App\PessoasModel;

class PessoasController extends Controller {

    public function index() {
        $pessoas = PessoasModel::all();
        //return view('pessoas.index', ['pessoasAll' => $pessoas]);
        //return $pessoas;
        return view('pessoas.index')->with('pessoasAll', $pessoas);
    }

    public function novo() {
        //$pessoa = PessoasModel::select('*')->where('id', $id)->get();
        return view('pessoas.novo');
    }

    public function salvar(Request $request) {

        $pessoas = new PessoasModel;
        $pessoas->name = $request->name;
        $pessoas->endereco = $request->endereco;
        $pessoas->save();

        return redirect('pessoas');
    }

    public function edit($id) {
        $pessoa = PessoasModel::find($id);
        //return view('pessoas.index', ['pessoasAll' => $pessoas]);
        //return $pessoa;
        return view('pessoas.edit')->with('pessoa', $pessoa);
    }

    public function update(Request $request, $id) {

        $pessoas = PessoasModel::find($id);
        $pessoas->name = $request->name;
        $pessoas->endereco = $request->endereco;
        $pessoas->update();

        return redirect('pessoas');
    }

    
    public function delete($id) {

        $pessoas = PessoasModel::find($id);
        $pessoas->delete();

        return redirect('pessoas');
    }
}
