@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <?php //Session::get('message') ?>
                    <a href="/pessoas/novo" class="btn btn-primary pull-right">
                        Cadastrar Pessoa
                    </a>
                    <h1>Pessoas</h1>

                    <table class="table table-bordered table-striped table-condensed table-hover">
                        <?php foreach ($pessoasAll as $produto) { ?>
                            <tr>
                                <td>{{$produto->name}}</td>
                                <td>{{$produto->endereco}}</td>
                                <td>
                                    <a href="/pessoas/edit/<?php echo $produto->id ?>" class="btn btn-primary btn-xs">
                                        Editar
                                    </a>
                                    <a href="/pessoas/delete/<?php echo $produto->id ?>" class="btn btn-danger btn-xs">
                                        Deletar
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
