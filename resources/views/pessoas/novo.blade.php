@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <?php //Session::get('message') ?>
                    <a href="/pessoas/" class="btn btn-primary pull-right">
                        Voltar
                    </a>
                    <h1>Pessoas</h1>

                    <div class="form-horizontal">
                        <form action="/pessoas/novo" method="post">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <label>Nome</label>
                                    <input type="text" name="name" class="form-control" required="true">
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    <label>Endereço</label>
                                    <input type="text" name="endereco" class="form-control">
                                </div>

                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <div class="col-md-8">

                                    <input type="submit" value="Cadastrar" class="form-control btn btn-primary">
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
