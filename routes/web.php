    <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});



Route::get('/home', 'HomeController@index')->name('home');
Route::get('pessoas','PessoasController@index');
Route::get('pessoas/novo','PessoasController@novo');
Route::post('pessoas/novo','PessoasController@salvar');
Route::get('pessoas/edit/{id}','PessoasController@edit');
Route::post('pessoas/edit/{id}','PessoasController@update');
Route::get('pessoas/delete/{id}','PessoasController@delete');

Auth::routes();